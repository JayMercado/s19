// Allows use of featureas for web applications
const { json } = require("express");
const express = require("express");
const mongoose = require("mongoose");

// Save the express appliaction as a constant named app
const app = express();
const port = 3000;

// Used in handling json request bodies
app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.listen(port, () => console.log(`You got served on port ${port}`));

// MongoDB Atlas
mongoose.connect(
  "mongodb+srv://admin:admin123@cluster0.khzs2.mongodb.net/to_do_list?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  }
);

// // local MongoDB
// mongoose.connect('mongodv://localhost:27017/to_do_list',{
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//     useFindAndModify: false
// })

let db = mongoose.connection;

// if connection error encountered, print message in console
db.on("error", console.log.bind(console, "connection error"));

// One connected it will print success message in console
db.once("open", () => console.log("We're connected to our cloud database"));

// Define a task schema/ blueprint
// it defines the structure of our task document
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

/*
sample task document
{
    name: 'eat',
    status: 'pending'
}
*/

const userSchema = new mongoose.Schema({
  username: String,
  email: String,
  password: String,
  // tasks in an array of embedded/ nested task objects
  tasks: [taskSchema],
});

// In the context of mongoose, a model is a class used in constructing documents
const Task = mongoose.model("Task", taskSchema);
const User = mongoose.model("User", userSchema);

// API/ routes
app.get("/", (req, res) => {
  res.send("Hello world");
});

// Create a user
app.post("/users", (req, res) => {
  // Check for duplicate username or email
  User.find(
    {
      $or: [
        { username: req.body.username },
        { email: req.body.email }
      ],
    },
    (findErr, duplicates) => {
      if (findErr) {
        console.error(findErr);
      }

      if (duplicates.length > 0) {
        return res.status(403).json({
          message: "Duplicates found, kindly choose a different and/ or email",
        });
      } else {
        let newUser = new User({
          username: req.body.username,
          email: req.body.email,
          password: req.body.password,
          tasks: [],
        });

        newUser.save((saveErr, newUser) => {
          if (saveErr) {
            return console.error(saveErr);
          }

          return res.status(201).json({
            message: `User ${newUser.username} successfully registered.`,
            data: {
              username: newUser.username,
              email: newUser.email,
              link_to_self: `/user/${newUser._id}`,
            },
          });
        });
      }
    }
  );
});

// Show particular user
app.get("/users/:id", (req, res) => {
  User.findById(req.params.id, (err, user) => {
    if (err) {
      return console.error(err)
    }
    return res.status(200).json({
      message: "User retrieved successfully",
      data: {
        username: {
          username: user.username,
          email: user.email,
          tasks: `/users/${user.id}/tasks`
        }
      }
    })
  })
});

// Create a new task for a particular user
app.post('/users/:userId/tasks', (req, res) => {
  User.findById(req.params.userId, (findErr, user) => {

    if (findErr) {
      return console.error(findErr);
    };

    /*{
    username:"john1",
    email:"john@gmail.com",
    password:"john1234",
    tasks: ["eat"]
    }*/
    

    if (user.tasks.length === 0) {

      user.tasks.push({
        name: req.body.name
      });

      user.save((saveErr, modifiedUser) => {
        if (saveErr) {
          return console.error(saveErr);
        };

        return res.status(200).json({
          message: `${user.tasks[0].name} added to task list of ${user.username}`,
          data: modifiedUser.tasks[0]
        })
      })
    } else {
      let duplicates = user.tasks.filter(task =>
        task.name.toLowerCase() === req.body.name.toLowerCase()
      );

      if (duplicates.length > 0) {
        return res.status(403).json({
          message: `${req.body.name} is already registered as a task.`
        })
      } else {
        user.tasks.push({
          name: req.body.name
        })

        user.save((saveErr, modifiedUser) => {
          if (saveErr) {
            return console.error(saveErr)
          }
          return res.status(200).json({
            message: `${modifiedUser.tasks[modifiedUser.tasks.length - 1].name} added to task list of ${user.username}`,
            data: modifiedUser.tasks[modifiedUser.tasks.length - 1]
          })
        })
      }
    }
  });
});

// Get details of a aspecific task of a user
app.get('/users/:userId/tasks/:taskId', (req, res) => {
  User.findById(req.params.userId, (findErr, user) => {

    if (findErr) {
      return console.error(findErr);
    };

    let task = user.tasks.id(req.params.taskId);

    if(task === null){
      return res.status(403).json({
        message:`Task with id ${req.params.taskId} cannot be found`
      })
    } 

    if (user.tasks.length > 0) {
      return res.status(200).json({
        message: `Tasks of ${user.username} retrieved successfully.`,
        data: user.tasks.id(req.params.taskId)
        // data: task
      })
    } else {
      return res.status(200).json({
        message: `${user.username} currently has no tasks.`
      })
    }
  })
})

// Activity
// 1. Implement the route for getting all tasks of a particular user
// app.get('/users/:userId/tasks', (req, res) => {
//   User.findById(req.params.userId, (findErr, user) => {

//     if (findErr) {
//       return console.error(findErr);
//     };

//     if (user.tasks.length > 0) {
//       return res.status(200).json({
//         message: `Tasks of ${user.username} retrieved successfully.`,
//         data: user.tasks
//       })
//     } else {
//       return res.status(200).json({
//         message: `${user.username} currently has no tasks.`
//       })
//     }
//   })
// })

// 2. Implement a route for updating a task. (app.put)
// 	- Create the route for updating a task ('/users/:userId/tasks/:taskId')
// 	- Find the user to update
// 	- Add an if statement to catch any errors
// 	- Find the task to update
// 	- If the task does not exist return an error
// 	- Else update the task and save to the database
// app.put('/users/:userId/tasks/:taskId', (req, res) => {
//   User.findById(req.params.userId, (findErr, user) => {

//     if (findErr) {
//       return console.error(findErr);
//     };

//     let task = user.tasks.id(req.params.taskId);
//     let oldTask = task.name;

//     if(task === null){
//       return res.status(403).json({
//         message:`Task with id ${req.params.taskId} cannot be found`
//       })
//     } 

//     task.name = req.body.name;
//     task.status = req.body.status;

//     user.save((saveErr, modifiedUser) => {
//       if (saveErr) {
//         return console.error(saveErr)
//       }
//       return res.status(200).json({
//         message: `task: ${oldTask} of ${user.username} updated to ${task.name}`,
//         data: task
//       })
//     })
//   })
// })

// 3. Implement a route for deleting a task. (app.delete / .remove())
// - Create the route for updating a task ('/users/:userId/tasks/:taskId')
// - Find the user to update
// - Add an if statement to catch any errors
// - Find the task to update
// - Delete the task (.remove())
// - Save to the database
// app.delete('/users/:userId/tasks/:taskId', (req, res) => {
//   User.findById(req.params.userId, (findErr, user) => {

//     if (findErr) {
//       return console.error(findErr);
//     };

//     let task = user.tasks.id(req.params.taskId);

//     task.remove();

//     user.save((saveErr, modifiedUser) => {
//       if (saveErr) {
//         return console.error(saveErr)
//       }
//       return res.status(200).json({
//         message: `task of ${user.username} removed.`,
//         data: modifiedUser
//       })
//     })
//   })
// })

// 1. Implement the route for getting all tasks of a particular user.

app.get('/users/:userId/tasks', (req, res) => {
  //retrieve user who owns tasks to be viewed
  User.findById(req.params.userId, (err, user) => {
    if(err) {
      return console.error(err);
    }
    //let tasks = user.tasks;
    if(user.tasks.length > 0){
      return res.status(200).json({
        message: `Tasks of ${user.username} retrieved successfully.`,
        data: user.tasks
      })
    }else{
      return res.status(200).json({
        message: `${user.username} currently has no tasks.`
      })
    }
  })
})

// 2. Implement a route for updating a task.

app.put('/users/:userId/tasks/:taskId', (req, res) => {
  User.findById(req.params.userId, (findErr, user) => {
    if(findErr) {
      return console.error(findErr);
    }

    let task = user.tasks.id(req.params.taskId);

    if(task === null){
      return res.status(403).json({
        message: `Task with id ${req.params.taskId} cannot be found`
      })
    }else{
      if((typeof req.body.name === "string" && req.body.name !== "") && (typeof req.body.status === "string" && req.body.status !== "")){
        task.name = req.body.name;
        task.status = req.body.status;
        user.save((saveErr, modifiedUser) => {
          if(saveErr) return console.error(saveErr);
          return res.status(200).json({
            message: `Task with id ${req.params.taskId} updated successfully.`,
            data: task
          })
        }) 
      }else{
        return res.status(403).json({
          message: "Task name and task status are both required fields."
        })
      }
    }
  })
})

// 3. Implement a route for deleting a task.

app.delete('/users/:userId/tasks/:taskId', (req, res) => {
  User.findById(req.params.userId, (findErr, user) => {
    if(findErr) {
      return console.error(findErr);
    }
    
    user.tasks.id(req.params.taskId).remove();
    user.save((saveErr, modifiedUser) => {
      if(saveErr) return console.error(saveErr);
      return res.status(200).json({
        message: `Task with id ${req.params.taskId} deleted successfully.`,
        data: modifiedUser.tasks
      })
    })
  })
})